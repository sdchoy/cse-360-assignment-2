/**
* @author		Stephen Choy
* @classID	    348
* @Assignment 	2
* 
* Program that allows users to add, subtract, multiply, and divide numbers.
* Also returns the history of the user's actions.
*/
package cse360assign2;

/**
* This is the class for the calculator program. It has
* 2 variables, one for the total and the other for user's 
* history.
*/
public class Calculator 
{

	private int total;
	private String history;
	
	/**
    * This is the default constructor, used to initialize the total number and history string.
    * @param Nothing
    * @return Nothing
    */
	public Calculator() 
	{
		total = 0;  // not needed - included for clarity
		history = "0 "; //initial value
	}
	
	/**
    * This method is used to print the current total
    * @param Nothing
    * @return An integer which represents the current total
    */
	public int getTotal() 
	{
		return total;
	}
	
	/**
    * This method adds an int to the total 
    * @param value Integer to be added
    * @return Nothing
    */
	public void add(int value) 
	{
		total = total + value; //add
		history = history + "+ " + value + " ";
	}
	
	/**
    * This method subtracts the total by an int
    * @param value Integer that subtracts total
    * @return Nothing
    */
	public void subtract(int value) 
	{
		total = total - value; //subtract
		history = history + "- " + value + " ";
	}
	
	/**
    * This method multiplies the total by an int
    * @param value Integer that multiplies total
    * @return Nothing
    */
	public void multiply(int value) 
	{
		total = total * value; //multiply
		history = history + "* " + value + " ";
	}
	
	/**
    * This method divides the total by an int 
    * @param value Integer that divides total 
    * @return Nothing
    */
	public void divide(int value) 
	{
		if(value == 0) //check for divide by 0
		{
			total = 0;
			history = history + "/ " + value + " ";
		}
		else
		{
			total = total / value;
			history = history + "/ " + value + " ";
		}
	}
	
	/**
    * This method returns the history of the user's actions 
    * @param Nothing 
    * @return String Details user's actions
    */
	public String getHistory() 
	{
		return history;
	}
}